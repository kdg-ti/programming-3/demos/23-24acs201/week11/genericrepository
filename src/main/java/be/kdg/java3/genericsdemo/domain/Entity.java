package be.kdg.java3.genericsdemo.domain;

public interface Entity {
    int getId();
    void setId(int id);
}
