package be.kdg.java3.genericsdemo;

import be.kdg.java3.genericsdemo.presentation.StudentMenu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GenericsdemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(GenericsdemoApplication.class, args);
        context.getBean(StudentMenu.class).show();
    }

}
