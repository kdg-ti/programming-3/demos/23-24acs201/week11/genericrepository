package be.kdg.java3.genericsdemo.repository;

import be.kdg.java3.genericsdemo.domain.School;

import java.util.List;

public interface SchoolRepository extends EntityRepository<School> {
}
