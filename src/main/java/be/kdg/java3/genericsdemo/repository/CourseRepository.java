package be.kdg.java3.genericsdemo.repository;

import be.kdg.java3.genericsdemo.domain.Course;

import java.util.List;

public interface CourseRepository extends EntityRepository<Course>{
}
