package be.kdg.java3.genericsdemo.repository.jdbc;

import be.kdg.java3.genericsdemo.domain.Course;
import be.kdg.java3.genericsdemo.repository.CourseRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
@Profile("jdbctemplate")
public class JDBCCourseRepository extends JDBCRepository<Course> implements CourseRepository {
    public JDBCCourseRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    String getTableName() {
        return "COURSES";
    }

    @Override
    Map<String, Object> getParameters(Course course) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", course.getName());
        parameters.put("ACADEMIC_YEAR", course.getAcademicYear());
        return parameters;
    }

    @Override
    Course mapEntityRow(ResultSet rs, int rowid) throws SQLException {
        return new Course(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getInt("ACADEMIC_YEAR"));
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE COURSE_ID=?", id);
        super.delete(id);
    }
}
