package be.kdg.java3.genericsdemo.repository;

import be.kdg.java3.genericsdemo.domain.Teacher;

public interface TeacherRepository extends EntityRepository<Teacher> {
}
