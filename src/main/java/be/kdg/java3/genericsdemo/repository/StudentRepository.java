package be.kdg.java3.genericsdemo.repository;

import be.kdg.java3.genericsdemo.domain.Student;

import java.util.List;

public interface StudentRepository extends EntityRepository<Student>{
    List<Student> findBySchool(int schoolid);
    List<Student> findByCourse(int courseId);
}
