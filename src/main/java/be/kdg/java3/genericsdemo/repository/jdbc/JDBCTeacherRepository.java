package be.kdg.java3.genericsdemo.repository.jdbc;

import be.kdg.java3.genericsdemo.domain.Teacher;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class JDBCTeacherRepository extends JDBCRepository<Teacher> {
    public JDBCTeacherRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    String getTableName() {
        return "TEACHERS";
    }

    @Override
    Map<String, Object> getParameters(Teacher entity) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", entity.getName());
        parameters.put("SPECIALITY", entity.getSpeciality());
        return parameters;
    }

    @Override
    Teacher mapEntityRow(ResultSet rs, int rowid) throws SQLException {
        return new Teacher(rs.getInt("ID"), rs.getString("NAME"), rs.getString("SPECIALITY"));
    }
}
